#!/usr/bin/env bash

wget https://dl.fedoraproject.org/pub/epel/epel-release-latest-7.noarch.rpm
sudo rpm -Uvh epel-release-latest-7*.rpm
rm -Rf epel-release-latest-7*.rpm